import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  NumberValueAccessor,
  Validators,
} from "@angular/forms";
import { AppService } from "../app.service";
import { Router } from "@angular/router";
import { Location } from "@angular/common";

// This interface may be useful in the times ahead...
export interface Member {
  id: number;
  firstName: string;
  lastName: string;
  jobTitle: string;
  team: string;
  status: string;
}

@Component({
  selector: "app-member-details",
  templateUrl: "./member-details.component.html",
  styleUrls: ["./member-details.component.css"],
})
export class MemberDetailsComponent implements OnInit {
  memberModel: Member;
  memberForm: FormGroup;
  submitted = false;
  alertType: String;
  alertMessage: String;
  teams = [];
  selectedMember: any;

  constructor(
    private formBuilder: FormBuilder,
    private appService: AppService,
    private router: Router,
    private location: Location,
  ) {}

  ngOnInit(): void {
    this.memberForm = this.formBuilder.group({
      id: [''],
      firstName: ["", Validators.required],
      lastName: ["", Validators.required],
      jobTitle: ["", Validators.required],
      team: ["", Validators.required],
      status: ["", Validators.required],
    });
    this.selectedMember = ((this.location.getState() || {}) as any).member;
    this.appService.getTeams().subscribe(teams => {
      this.teams = teams;
      this.selectedMember && this.memberForm.patchValue(this.selectedMember);
    });
    
  }

  /**
   * Creates or updates the member details by making a service call
   * @param form Takes in formGroup of the member
   */
  onSubmit(form: FormGroup): void {
    (this.selectedMember && this.selectedMember.id
      ? this.appService
          .updateMember({ ...this.selectedMember, ...form.value })
      : this.appService.createMember(form.value)).subscribe(
        member => this.router.navigate(['members'])
      );
  }
}
