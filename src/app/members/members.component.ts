import { Component, OnInit } from "@angular/core";
import { AppService } from "../app.service";
import { Router } from "@angular/router";
import { Member } from "../member-details/member-details.component";

@Component({
  selector: "app-members",
  templateUrl: "./members.component.html",
  styleUrls: ["./members.component.css"],
})
export class MembersComponent implements OnInit {
  members: Array<Member> = [];

  constructor(
    public appService: AppService,
    private router: Router
  ) {}

  /**
   * Initiates service call for loading the members
   */
  ngOnInit(): void {
    this.getMembers();
  }

  /**
   * Navigates user to edit memeber page by passing the member information through route
   * @param id Member id of member that needs to be updated
   */
  editMember(id: number): void {
    const selectedMember = this.members.find((m) => m.id === id) as Member;
    this.navigateToMemberDetailsPage(selectedMember);
  }

  /**
   * Retrieves members and stores in local variable
   */
  getMembers(): void {
    this.appService
      .getMembers()
      .subscribe((members) => (this.members = members));
  }

  /**
   * Deletes member by id
   * @param id Member Id that needs to be deleted
   */
  deleteMember(id: number): void {
    this.appService.deleteMember(id).subscribe(() => this.getMembers());
  }

  /**
   * Navigates user to member details page
   * @param member Selected member data that is passed down to the next route
   */
  navigateToMemberDetailsPage(member?: Member): void {
    this.router.navigate(["member-details"], { state: { member }});
  }
}
