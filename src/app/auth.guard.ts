import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from "@angular/router";
import { Observable } from "rxjs";
import { getUserName } from "./utils/app.utils";

@Injectable({
  providedIn: "root",
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router) {}

  /**
   * Validates the route to see if we can route the user to requested route
   * and if not the user will be redirected to landing page
   * @param next ActivatedRouteSnapshot
   * @param state ActivatedRouteSnapshot
   * @returns boolean
   */
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const userName: string = getUserName();
    if (!userName) {
      this.router.navigate(["/"]);
      return false;
    }
    return true;
  }
}
