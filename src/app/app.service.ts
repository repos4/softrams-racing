import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Member } from './member-details/member-details.component';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  readonly api = 'http://localhost:8000/api';
  username: string;

  constructor(private http: HttpClient) {}

  /**
   * Returns all the members
   * @returns Observable<Array<Member>>
   */
  getMembers(): Observable<Array<Member>> {
    return this.http
      .get<Array<Member>>(`${this.api}/members`)
      .pipe(catchError(this.handleError));
  }

  setUsername(name: string): void {
    this.username = name;
  }

  /**
   * Creates member by making a service call
   * @param member Member request to be created
   * @returns Observable<Member> Returns Observable of member that is returned from the service
   */
  createMember(member: Member): Observable<Member> {
    return this.http
      .post<Member>(`${this.api}/members`, member)
      .pipe(catchError(this.handleError));
  }

  /**
   * Updates an existing member
   * @param member Member to be updated
   * @returns Observable<Member> Returns the updated Member details
   */
  updateMember(member: Member): Observable<Member> {
    return this.http
      .put<Member>(`${this.api}/members/${member.id}`, member)
      .pipe(catchError(this.handleError));
  }

  /**
   * Deletes a member from database by making a service call that takes in a member id
   * @param id Id of a member that needs to be delete
   * @returns 
   */
  deleteMember(id): Observable<void>{
    return this.http
      .delete(`${this.api}/members/${id}`)
      .pipe(catchError(this.handleError));
  }

  /**
   * Retrieves Teams from service that is displayed in the selecte dropdown
   * @returns Array of team 
   */
  getTeams(): Observable<Array<any>> {
    return this.http
      .get(`${this.api}/teams`)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    return [];
  }
}
