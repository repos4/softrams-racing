/**
 * Retrieves username from localstorage
 * @returns User name
 */
const getUserName = () => localStorage.getItem("username");

export { getUserName }